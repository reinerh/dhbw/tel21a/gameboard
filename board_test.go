package gameboard

import "fmt"

func ExampleRow() {
	l1 := []string{"123", "456", "789"}
	fmt.Println(Row(l1, 0))
	fmt.Println(Row(l1, 1))
	fmt.Println(Row(l1, 2))

	// Output:
	// 123
	// 456
	// 789
}

func ExampleColumn() {
	l1 := []string{"123", "456", "789"}
	fmt.Println(Column(l1, 0))
	fmt.Println(Column(l1, 1))
	fmt.Println(Column(l1, 2))

	// Output:
	// 147
	// 258
	// 369
}

func ExampleDiagDown() {
	l1 := []string{"123", "456", "789"}
	fmt.Println(DiagDown(l1))

	// Output:
	// 159
}

func ExampleDiagDownFromColumn() {
	l1 := []string{"123", "456", "789"}
	fmt.Println(DiagDownFromColumn(l1, 0))
	fmt.Println(DiagDownFromColumn(l1, -1))
	fmt.Println(DiagDownFromColumn(l1, -2))
	fmt.Println(DiagDownFromColumn(l1, 1))
	fmt.Println(DiagDownFromColumn(l1, 2))

	// Output:
	// 159
	// 48
	// 7
	// 26
	// 3
}

func ExampleDiagUp() {
	l1 := []string{"123", "456", "789"}
	fmt.Println(DiagUp(l1))

	// Output:
	// 753
}

func ExampleDiagUpFromColumn() {
	l1 := []string{"123", "456", "789"}
	fmt.Println(DiagUpFromColumn(l1, 0))
	fmt.Println(DiagUpFromColumn(l1, -1))
	fmt.Println(DiagUpFromColumn(l1, -2))
	fmt.Println(DiagUpFromColumn(l1, 1))
	fmt.Println(DiagUpFromColumn(l1, 2))

	// Output:
	// 753
	// 42
	// 1
	// 86
	// 9
}

func ExampleNewBoard() {
	b1 := NewBoard(3, 3, '*')
	b2 := NewBoard(3, 5, '*')
	b3 := NewBoard(1, 3, '*')

	fmt.Println(b1)
	fmt.Println(b2)
	fmt.Println(b3)

	// Output:
	// [*** *** ***]
	// [***** ***** *****]
	// [***]
}

func ExampleRowAsRunes() {
	b1 := NewBoard(3, 3, '*')
	fmt.Println(b1)

	Put(b1, 1, 1, '+')
	fmt.Println(b1)

	b2 := Put(b1, 2, 2, '+')
	fmt.Println(b1)
	fmt.Println(b2)

	// Output:
	// [*** *** ***]
	// [*** *+* ***]
	// [*** *+* **+]
	// [*** *+* **+]
}
