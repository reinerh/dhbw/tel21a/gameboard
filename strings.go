package gameboard

// Erwartet einen String und ein Zeichen r.
// Liefert true, falls der String ausschließlich r enthält.
func ContainsOnly(s string, r rune) bool {
	for _, v := range s {
		if v != r {
			return false
		}
	}
	return true
}

// Erwartet einen String und ein Zeichen r.
// Liefert true, falls der String wenigstens ein Vorkommen von r enthält.
func Contains(s string, r rune) bool {
	return ContainsN(s, 1, r)
}

// Erwartet einen String, eine Zahl n und ein Zeichen r.
// Liefert true, falls der String n zusammenhängende Vorkommen von r enthält.
func ContainsN(s string, n int, r rune) bool {
	count := 0
	for _, v := range s {
		if v == r {
			count++
			if count == n {
				return true
			}
		} else {
			count = 0
		}
	}
	return false
}
