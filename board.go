package gameboard

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Erwartet außerdem eine Zahl i.
// Liefert die i-te Zeile der Matrix als String.
func Row(l []string, i int) string {
	return l[i]
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Erwartet außerdem eine Zahl i.
// Liefert die i-te Spalte der Matrix als String.
func Column(l []string, i int) string {
	result := ""
	for _, row := range l {
		result += row[i : i+1]
	}
	return result
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Liefert die Diagonale der Matrix als String, die von links oben nach rechts unten
// geht und die in Spalte 0 startet.
func DiagDown(l []string) string {
	return DiagDownFromColumn(l, 0)
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Erwartet außerdem eine Zahl n.
// Liefert die Diagonale der Matrix als String, die von links oben nach rechts unten
// geht und die in Spalte n startet.
func DiagDownFromColumn(l []string, n int) string {
	result := ""
	for i := range l {
		row := i
		col := n + i
		if col >= 0 && col < len(l) {
			result += l[row][col : col+1]
		}
	}
	return result
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Liefert die Diagonale der Matrix als String, die von links unten nach rechts oben
// geht und die in Spalte 0 startet.
func DiagUp(l []string) string {
	return DiagUpFromColumn(l, 0)
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Erwartet außerdem eine Zahl n.
// Liefert die Diagonale der Matrix als String, die von links oben nach rechts unten
// geht und die in Spalte n startet.
func DiagUpFromColumn(l []string, n int) string {
	result := ""
	for i := range l {
		row := len(l) - i - 1
		col := n + i
		if col >= 0 && col < len(l) {
			result += l[row][col : col+1]
		}
	}
	return result
}

// Erwartet Höhe und Breite sowie ein Zeichen r.
// Liefert ein neues Spielfeld mit den gegebenen Dimensionen, das mit r initialisiert wird.
func NewBoard(height, width int, r rune) []string {
	board := make([]string, 0)
	for row := 0; row < height; row++ {
		currentRow := ""
		for col := 0; col < width; col++ {
			currentRow += string(r)
		}
		board = append(board, currentRow)
	}
	return board
}

// Erwartet eine Liste von Strings, die als Spielfeld aufgefasst wird.
// Erwartet außerdem Koordinaten row und col sowie ein Zeichen.
// Setzt das Zeichen r an die angegebene Stelle und liefert das resultierende Board.
//
// Hinweis: Die Funktion hat einen Seiteneffekt, d.h. sie verändert das gegebene Board.
func Put(board []string, row, col int, r rune) []string {
	rowAsRunes := []rune(Row(board, row))
	rowAsRunes[col] = r
	board[row] = string(rowAsRunes)
	return board
}
