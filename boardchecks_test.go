package gameboard

import "fmt"

func ExampleNoneContains() {
	l1 := []string{"aaa", "bbb"}
	l2 := []string{"aa a", "bb b"}
	l3 := []string{"", "b"}
	l4 := []string{""}
	l5 := []string{" "}
	fmt.Println(NoneContains(l1, ' '))
	fmt.Println(NoneContains(l2, ' '))
	fmt.Println(NoneContains(l3, ' '))
	fmt.Println(NoneContains(l4, ' '))
	fmt.Println(NoneContains(l5, ' '))

	// Output:
	// true
	// false
	// true
	// true
	// false
}

func ExampleAnyContainsOnly() {
	b1 := []string{"X X", "OOO", " XO"}
	b2 := []string{"X  ", " X ", "  X"}

	fmt.Println(AnyContainsOnly(b1, 'X'))
	fmt.Println(AnyContainsOnly(b1, 'O'))
	fmt.Println(AnyContainsOnly(b2, 'X'))
	fmt.Println(AnyContainsOnly(b2, 'O'))

	// Output:
	// false
	// true
	// false
	// false
}

func ExampleAnyRowContainsOnly() {
	b1 := []string{"X X", "OOO", " XO"}
	b2 := []string{"X  ", " X ", "  X"}

	fmt.Println(AnyRowContainsOnly(b1, 'X'))
	fmt.Println(AnyRowContainsOnly(b1, 'O'))
	fmt.Println(AnyRowContainsOnly(b2, 'X'))
	fmt.Println(AnyRowContainsOnly(b2, 'O'))

	// Output:
	// false
	// true
	// false
	// false
}

func ExampleAnyColumnContainsOnly() {
	b1 := []string{"X X", "XOO", "X O"}
	b2 := []string{"X  ", " X ", "  X"}

	fmt.Println(AnyColumnContainsOnly(b1, 'X'))
	fmt.Println(AnyColumnContainsOnly(b1, 'O'))
	fmt.Println(AnyColumnContainsOnly(b2, 'X'))
	fmt.Println(AnyColumnContainsOnly(b2, 'O'))

	// Output:
	// true
	// false
	// false
	// false
}

func ExampleAnyDiagContainsOnly() {
	b1 := []string{"X X", "OOO", " XO"}
	b2 := []string{"X  ", " X ", "  X"}

	fmt.Println(AnyDiagContainsOnly(b1, 'X'))
	fmt.Println(AnyDiagContainsOnly(b1, 'O'))
	fmt.Println(AnyDiagContainsOnly(b2, 'X'))
	fmt.Println(AnyDiagContainsOnly(b2, 'O'))

	// Output:
	// false
	// false
	// true
	// false
}

func ExampleAnyContainsNInARow() {
	b1 := []string{"X X", "OOO", " XO"}
	b2 := []string{"X  ", " X ", "  X"}

	fmt.Println(AnyContainsNInARow(b1, 1, 'X'))
	fmt.Println(AnyContainsNInARow(b1, 2, 'X'))
	fmt.Println(AnyContainsNInARow(b1, 2, 'O'))
	fmt.Println(AnyContainsNInARow(b2, 1, 'X'))
	fmt.Println(AnyContainsNInARow(b2, 2, 'O'))

	// Output:
	// true
	// false
	// true
	// true
	// false
}

func ExampleAnyRowContainsNInARow() {
	b1 := []string{"X X", "OOO", " XO"}
	b2 := []string{"X  ", " X ", "  X"}

	fmt.Println(AnyRowContainsNInARow(b1, 2, 'X'))
	fmt.Println(AnyRowContainsNInARow(b1, 2, 'O'))
	fmt.Println(AnyRowContainsNInARow(b1, 3, 'O'))
	fmt.Println(AnyRowContainsNInARow(b1, 4, 'O'))
	fmt.Println(AnyRowContainsNInARow(b2, 2, 'X'))
	fmt.Println(AnyRowContainsNInARow(b2, 2, 'O'))

	// Output:
	// false
	// true
	// true
	// false
	// false
	// false
}

func ExampleAnyColumnContainsNInARow() {
	b1 := []string{"X X", "XOO", "X O"}
	b2 := []string{"X  ", " X ", "  X"}

	fmt.Println(AnyColumnContainsNInARow(b1, 2, 'X'))
	fmt.Println(AnyColumnContainsNInARow(b1, 3, 'X'))
	fmt.Println(AnyColumnContainsNInARow(b1, 4, 'X'))
	fmt.Println(AnyColumnContainsNInARow(b1, 2, 'O'))
	fmt.Println(AnyColumnContainsNInARow(b2, 1, 'X'))
	fmt.Println(AnyColumnContainsNInARow(b2, 1, 'O'))

	// Output:
	// true
	// true
	// false
	// true
	// true
	// false
}

func ExampleAnyDiagContainsNInARow() {
	b1 := []string{"X X", "OXO", "  X"}
	b2 := []string{"   ", "X  ", " X "}

	fmt.Println(AnyDiagContainsNInARow(b1, 2, 'X'))
	fmt.Println(AnyDiagContainsNInARow(b1, 3, 'X'))
	fmt.Println(AnyDiagContainsNInARow(b1, 3, '0'))
	fmt.Println(AnyDiagContainsNInARow(b2, 2, 'X'))
	fmt.Println(AnyDiagContainsNInARow(b2, 3, 'X'))

	// Output:
	// true
	// true
	// false
	// true
	// false
}
