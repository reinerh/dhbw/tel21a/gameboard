package gameboard

// Erwartet eine Liste von Strings und ein Zeichen r.
// Liefert true, falls keiner der Strings r enthält.
func NoneContains(l []string, r rune) bool {
	for _, line := range l {
		if Contains(line, r) {
			return false
		}
	}
	return true
}

// Erwartet eine Liste von Strings und ein Zeichen r.
// Liefert true, falls einer der Strings ausschließlich r enthält.
func AnyContainsOnly(b []string, r rune) bool {
	for _, s := range b {
		if ContainsOnly(s, r) {
			return true
		}
	}
	return false
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Erwartet außerdem ein Zeichen r.
// Liefert true, falls eine der Zeilen ausschließlich r enthält.
func AnyRowContainsOnly(b []string, r rune) bool {
	return AnyContainsOnly(b, r)
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Erwartet außerdem ein Zeichen r.
// Liefert true, falls eine der Spalten ausschließlich r enthält.
// Annahme: Alle Zeilen der Matrix sind gleich lang.
//          Ist dies nicht der Fall, wird sich die Funktion nicht wie erwartet verhalten.
func AnyColumnContainsOnly(b []string, r rune) bool {
	columnCount := len(b[0])
	columns := make([]string, columnCount)
	for i := 0; i < columnCount; i++ {
		columns[i] = Column(b, i)
	}
	return AnyContainsOnly(columns, r)
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Erwartet außerdem ein Zeichen r.
// Liefert true, falls eine der Diagonalen ausschließlich r enthält.
// Annahme: Die Matrix ist quadratisch.
func AnyDiagContainsOnly(b []string, r rune) bool {
	diags := []string{DiagDown(b), DiagUp(b)}
	return AnyContainsOnly(diags, r)
}

// Erwartet eine Liste von Strings, eine Zahl n und ein Zeichen r.
// Liefert true, falls einer der Strings ausschließlich n aufeinanderfolgende gleiche
// r enthält.
func AnyContainsNInARow(b []string, n int, r rune) bool {
	for _, s := range b {
		if ContainsN(s, n, r) {
			return true
		}
	}
	return false
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Erwartet außerdem eine Zahl n und ein Zeichen r.
// Liefert true, falls eine der Zeilen n aufeinanderfolgende r enthält.
func AnyRowContainsNInARow(b []string, n int, r rune) bool {
	return AnyContainsNInARow(b, n, r)
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Erwartet außerdem eine Zahl n und ein Zeichen r.
// Liefert true, falls eine der Spalten n aufeinanderfolgende r enthält.
// Annahme: Alle Zeilen der Matrix sind gleich lang.
//          Ist dies nicht der Fall, wird sich die Funktion nicht wie erwartet verhalten.
func AnyColumnContainsNInARow(b []string, n int, r rune) bool {
	columnCount := len(b[0])
	columns := make([]string, columnCount)
	for i := 0; i < columnCount; i++ {
		columns[i] = Column(b, i)
	}
	return AnyContainsNInARow(columns, n, r)
}

// Erwartet eine Liste von Strings, die als Matrix aus Buchstaben aufgefasst wird.
// Erwartet außerdem eine Zahl n und ein Zeichen r.
// Liefert true, falls eine der Diagonalen n aufeinanderfolgende r enthält.
// Annahme: Alle Zeilen der Matrix sind gleich lang.
//          Ist dies nicht der Fall, wird sich die Funktion nicht wie erwartet verhalten.
func AnyDiagContainsNInARow(b []string, n int, r rune) bool {
	columnCount := len(b[0])
	rowCount := len(b)
	diags := make([]string, 0)
	for i := -rowCount + 1; i < columnCount; i++ {
		diags = append(diags, DiagUpFromColumn(b, i))
		diags = append(diags, DiagDownFromColumn(b, i))
	}
	return AnyContainsNInARow(diags, n, r)
}
