package gameboard

import "fmt"

func ExampleContainsOnly() {
	fmt.Println(ContainsOnly("aaa", 'a'))
	fmt.Println(ContainsOnly("aaa", 'b'))
	fmt.Println(ContainsOnly("aba", 'b'))
	fmt.Println(ContainsOnly("", 'a'))

	// Output:
	// true
	// false
	// false
	// true
}

func ExampleContains() {
	fmt.Println(Contains("aaa", 'a'))
	fmt.Println(Contains("aaa", 'b'))
	fmt.Println(Contains("aba", 'b'))
	fmt.Println(Contains("", 'a'))

	// Output:
	// true
	// false
	// true
	// false
}

func ExampleContainsN() {
	fmt.Println(ContainsN("aaa", 2, 'a'))
	fmt.Println(ContainsN("aaa", 2, 'b'))
	fmt.Println(ContainsN("aba", 2, 'b'))
	fmt.Println(ContainsN("aba", 2, 'a'))
	fmt.Println(ContainsN("", 1, 'a'))
	fmt.Println(ContainsN("abbbabaaaabba", 4, 'a'))
	fmt.Println(ContainsN("abbbabaaaabba", 5, 'a'))

	// Output:
	// true
	// false
	// false
	// false
	// false
	// true
	// false
}
